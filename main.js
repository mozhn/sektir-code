"use strict";

const {app, BrowserWindow, Menu} = require('electron');
const path = require('path');
const url = require('url');
const fs = require('fs');
const storage = require('electron-storage');

let win;

function createWindow() {
   
  win = new BrowserWindow({show: false, 'minHeight': 600, 'minWidth': 850, icon: path.join(__dirname, 'src/images/icon.png')});
  win.maximize();
  win.show();
  win.setMenu(null);

  storage.isPathExists('settings.json').then(itDoes => {
    if (itDoes) {
      win.loadURL(url.format({
        pathname: path.join(__dirname, '/src/index.html'),
        protocol: 'file:',
        slashes: true
      })); 
    } else {
      win.loadURL(url.format({
        pathname: path.join(__dirname, '/src/start-page.html'),
        protocol: 'file:',
        slashes: true
      })); 
    }
  });

  //win.webContents.openDevTools();

  win.on('closed', () => {
      win = null
  });
}

app.on('ready', createWindow);

app.on('window-all-closed', () => {
 
  if (process.platform !== 'darwin') {
    app.quit()
  }
});

app.on('activate', () => {
 
  if (win === null) {
    createWindow()
  }
});