const fs = require('fs');
const storage = require('electron-storage');
const { dialog } = require('electron').remote;

if (typeof module === 'object') {
    window.module = module; module = undefined;
}

if (window.module) module = window.module;

var filePath;
var fileSaveStatus = true;