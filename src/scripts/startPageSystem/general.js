const storage = require('electron-storage');

storage.isPathExists('settings.json').then(itDoes => {
    if (!itDoes) {
      let object = { settings: [] };
      object.settings.push({ fontSize: 14, menuStatus: true, backgroundColor: "#282C34", textColor: "#ffffff", fontFamily: "Arial"});
  
      var jsonData = JSON.stringify(object);
  
      storage.set('settings.json', jsonData).then(() => {
        console.log("Ayarlat başarılı bir şekilde kayıt edildi.");
      }).catch(err => {
          alert("Bir hata oluştu. Lütfen geliştiriciye hatayı bildiriniz. Hata mesajı: " + err);
      });
  
    }
});


setTimeout(() => {
  document.getElementById("txt").innerText = "Editör hazır!";
  setTimeout(() => {
    document.getElementById("buton").style.display = "block";
  }, 500);
}, 4000);

document.getElementById("buton").addEventListener("click", function() {
  window.location.href = "index.html";
});
