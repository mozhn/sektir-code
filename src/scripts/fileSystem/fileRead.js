document.getElementById("read-file").addEventListener("click", function() {
    dialog.showOpenDialog((fileNames) => {
        
        if (fileNames === undefined) {
            return;
        }
    
        fs.readFile(fileNames[0], 'utf-8', (err, data) => {
            if(err){
                setTimeout(() => {
                    alert("Bir hata oluştu. Lütfen geliştiriciye hatayı bildiriniz. Hata mesajı: " + err.message);
                }, 1000);
                return;
            }

            if (fileSaveStatus == false) {
                $(".alert-window").css({"display": "block"});
                $(".alert-blocked").css({"display": "block"});

                return;
            }

            filePath = fileNames[0];

            editor.setValue(data);
            document.getElementById("tt").innerText = "SC - " + filePath;
            document.getElementById("file-name-and-message").innerText = filePath + " konumundaki dosyanızı kaydetmediniz! Lütfen bir işlem yapınız.";
        });
    });
});
