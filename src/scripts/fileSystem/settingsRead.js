const path = require('path');

storage.isPathExists('settings.json').then(itDoes => {
  if (itDoes) {
    storage.get('settings.json', (err, data) => {
        if (err) {
            alert("Bir hata oluştu. Lütfen geliştiriciye hatayı bildiriniz. Hata mesajı: " + err);
        } else {
            strMenuStatus = data.settings[0].menuStatus;
            strFontSize = data.settings[0].fontSize;
            strFontFamily = data.settings[0].fontFamily;
    
            $(".cm-s-default .cm-error").css({
                "color": data.settings[0].textColor
            });
    
            $(".codeMirror").css({
                "font-size": data.settings[0].fontSize,
                "background-color": data.settings[0].backgroundColor,
                "color": data.settings[0].textColor,
                //"font-family": data.settings[0].fontFamily
            });

            $("body").css({
                "font-family": data.settings[0].fontFamily
            });
    
            $(".CodeMirror-gutters").css({
                "font-size": data.settings[0].fontSize,
                "background-color": data.settings[0].backgroundColor,
                "color": data.settings[0].textColor
            });
    
            if (data.settings[0].menuStatus == true) {
                $(".menu").css({
                    "opacity": 1
                });
            } else if(data.settings[0].menuStatus == false) {
                $(".menu").css({
                    "opacity": 0
                });
            }
    
            if (data.settings[0].menuStatus == false) {
                $(".menu").hover(function(){
                    $(this).css("opacity", 1);
                }, function() {
                    $(this).css("opacity", 0);
                });
            }
        }
    });
  } else {
      console.log("henüz oluşturulmadı");
  }
});
