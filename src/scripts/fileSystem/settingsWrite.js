document.getElementById("settings-save").addEventListener("click", function(e) {
    e.preventDefault();

    var object = { settings: [] };

    var fntSize = document.getElementById("font-size").value;
    var menuStat = document.getElementById("menu-status").value;
    var fontFamily = document.getElementById("font-family").value;

    var newMenuStat = undefined;

    if (menuStat == "true") {
        newMenuStat = true;
    } else if (menuStat == "false") {
        newMenuStat = false;
    }

    object.settings.push({ fontSize: fntSize, menuStatus: newMenuStat, backgroundColor: "#282C34", textColor: "#ffffff", fontFamily: fontFamily});
    var jsonData = JSON.stringify(object);

    storage.set('settings.json', jsonData).then(() => {
        alert("Ayarlar başarılı bir şekilde kayıt edildi.");
    }).catch(err => {
        alert("Bir hata oluştu. Lütfen geliştiriciye hatayı bildiriniz. Hata mesajı: " + err);
    });

});