$(document).keydown(function(e) {
    if ((e.key == 's' || e.key == 'S' ) && (e.ctrlKey || e.metaKey)) {
        e.preventDefault();
        
        let content = editor.getValue();

        if (filePath == undefined) {
            dialog.showSaveDialog((filename) => {
                if (filename === undefined)
                    return;
                
                fs.writeFile(filename, content, (err) => {
                    if (err) {
                        setTimeout(() => {
                            alert("Bir hata oluştu. Lütfen geliştiriciye hatayı bildiriniz. Hata mesajı: " + err.message);
                        }, 1000);
                    }
        
                    filePath = filename;
                    fileSaveStatus = true;
                    document.getElementById("tt").innerText = "SC - " + filePath;
                    document.getElementById("file-name-and-message").innerText = filePath + " konumundaki dosyanızı kaydetmediniz! Lütfen bir işlem yapınız.";
                });
            });
        } else if (filePath != undefined) {
            fs.writeFile(filePath, content, (err) => {
                if (err) {
                    setTimeout(() => {
                        alert("Bir hata oluştu. Lütfen geliştiriciye hatayı bildiriniz. Hata mesajı: " + err.message);
                    }, 1000);
                }
                fileSaveStatus = true;
                document.getElementById("tt").innerText = "SC - " + filePath;
            });
        }

        return false;
    }
    
    return true;
}); 

document.getElementById("write-file").addEventListener("click", function() {
    let content = editor.getValue();

    if (filePath == undefined) {
        dialog.showSaveDialog((filename) => {
            if (filename === undefined)
                return;
            
            fs.writeFile(filename, content, (err) => {
                if (err) {
                    setTimeout(() => {
                        alert("Bir hata oluştu. Lütfen geliştiriciye hatayı bildiriniz. Hata mesajı: " + err.message);
                    }, 1000);
                }
    
                filePath = filename;
                fileSaveStatus = true;
                document.getElementById("tt").innerText = "SC - " + filePath;
                document.getElementById("file-name-and-message").innerText = filePath + " konumundaki dosyanızı kaydetmediniz! Lütfen bir işlem yapınız.";
            });
        });
    } else if (filePath != undefined) {
        fs.writeFile(filePath, content, (err) => {
            if (err) {
                setTimeout(() => {
                    alert("Bir hata oluştu. Lütfen geliştiriciye hatayı bildiriniz. Hata mesajı: " + err.message);
                }, 1000);
            }
            fileSaveStatus = true;
            document.getElementById("tt").innerText = "SC - " + filePath;
        });
    }
});

function save() {
    let content = editor.getValue();

    if (filePath == undefined) {
        dialog.showSaveDialog((filename) => {
            if (filename === undefined)
                return;
            
            fs.writeFile(filename, content, (err) => {
                if (err) {
                    setTimeout(() => {
                        alert("Bir hata oluştu. Lütfen geliştiriciye hatayı bildiriniz. Hata mesajı: " + err.message);
                    }, 1000);
                }
    
                fileSaveStatus = true;
                document.getElementById("tt").innerText = "SC - " + filePath;
                document.getElementById("file-name-and-message").innerText = filePath + " konumundaki dosyanızı kaydetmediniz! Lütfen bir işlem yapınız.";
            });
        });
    } else if (filePath != undefined) {
        fs.writeFile(filePath, content, (err) => {
            if (err) {
                setTimeout(() => {
                    alert("Bir hata oluştu. Lütfen geliştiriciye hatayı bildiriniz. Hata mesajı: " + err.message);
                }, 1000);
            }
            fileSaveStatus = true;
            document.getElementById("tt").innerText = "SC - " + filePath;
        });
    }
}