document.getElementById("new-file").addEventListener("click", function() {
    
    if (fileSaveStatus == false) {
        $(".alert-window").css({"display": "block"});
        $(".alert-blocked").css({"display": "block"});
    } else if (fileSaveStatus == true) {
        window.location.href = 'index.html';
    }

});

document.getElementById("settings-open").addEventListener("click", function() {
    
    if (fileSaveStatus == false) {
        $(".alert-window").css({"display": "block"});
        $(".alert-blocked").css({"display": "block"});
    } else if (fileSaveStatus == true) {
        window.location.href = 'settings.html';
    }

});

document.getElementById("alert-cancel").addEventListener("click", function() {
    $(".alert-window").css({"display": "none"});
    $(".alert-blocked").css({"display": "none"});
});

document.getElementById("alert-dontsave").addEventListener("click", function() {
    fileSaveStatus = true;
    
    editor.setValue("");
    
    $(".alert-window").css({"display": "none"});
    $(".alert-blocked").css({"display": "none"});
});

document.getElementById("alert-save").addEventListener("click", function() {
    save();
    fileSaveStatus = true;
    filePath = "";
    editor.setValue("");
    
    $(".alert-window").css({"display": "none"});
    $(".alert-blocked").css({"display": "none"});
});