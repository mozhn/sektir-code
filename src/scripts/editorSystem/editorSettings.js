var editor = CodeMirror(document.body, {
    mode: "vb",
    lineNumbers: true,
    extraKeys: {"Ctrl-Space": "autocomplete"},
    tabSize: 3
});

editor.setOption("smartIndent", false);

editor.on("inputRead", function() {
    if (fileSaveStatus != false) {
        fileSaveStatus = false;
        document.getElementById("tt").innerText = "* SC - " + filePath;
    }
});

