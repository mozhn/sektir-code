// CodeMirror, copyright (c) by Marijn Haverbeke and others
// Distributed under an MIT license: http://codemirror.net/LICENSE

(function(mod) {
  if (typeof exports == "object" && typeof module == "object") // CommonJS
    mod(require("../../lib/codemirror"));
  else if (typeof define == "function" && define.amd) // AMD
    define(["../../lib/codemirror"], mod);
  else // Plain browser env
    mod(CodeMirror);
})(function(CodeMirror) {
"use strict";

CodeMirror.defineMode("vb", function(conf, parserConf) {
    var ERRORCLASS = 'error';

    function wordRegexp(words) {
        return new RegExp("^((" + words.join(")|(") + "))\\b", "i");
    }

    var singleOperators = new RegExp("^[\\+\\-\\*/%&\\\\|\\^~<>!]");
    var singleDelimiters = new RegExp('^[\\(\\)\\[\\]\\{\\}@,:`=;\\.]');
    var doubleOperators = new RegExp("^((==)|(<>)|(<=)|(>=)|(<>)|(<<)|(>>)|(//)|(\\*\\*))");
    var doubleDelimiters = new RegExp("^((\\+=)|(\\-=)|(\\*=)|(%=)|(/=)|(&=)|(\\|=)|(\\^=))");
    var tripleDelimiters = new RegExp("^((//=)|(>>=)|(<<=)|(\\*\\*=))");
    var identifiers = new RegExp("^[_A-Za-z][_A-Za-z0-9]*");

    var skriptEvents = [
        'at', 'on bed enter', 'on bed leave', 'on block damage', 'on block growth', 'on book edit',
        'on book sign', 'on break', 'on mine', 'on bucket empty', 'on bucket fill', 'on burn',
        'on can build check', 'on chat', 'on chunk generate', 'on chunk load', 'on chunk unload',
        'on click', 'on combust', 'on command', 'on connect', 'on consume', 'on craft', 'on creeper power',
        'on damage', 'on death', 'on dispense', 'on drop', 'on enderman', 'on entity dismount', 'on entity mount',
        'on experience spawn', 'on explode', 'on explosion prime', 'on fade', 'on first join', 'on fishing',
        'on flow', 'on form', 'on fuel burn', 'on gamemode change', 'on gliding state change', 'on grow',
        'on heal', 'on hunter meter change', 'on ignition', 'on ignite', 'on inventory close', 'on inventory click',
        'on inventory open', 'on item break', 'on item spawn', 'on join', 'on kick', 'on leaves decay', 'on level change',
        'on lightning strike', 'on move on', 'on physics', 'on pick up', 'on pig zap', 'on piston extend', 'on piston retract',
        'on place', 'on player world change', 'on portal', 'on portal create', 'on portal enter', 'on prepare craft',
        'on pressure plate', 'on pressure trip', 'on projectile hit', 'on quit', 'on redstone', 'on region',
        'on respawn', 'on resurrect attempt', 'on load', 'on unload', 'on skript start', 'on skript load',
        'on skript enable', 'on server stop', 'on server unload', 'on server disable', 'on sheep regrow wool',
        'on shoot', 'on sign change', 'on slime split', 'on smelt', 'on sneak toggle', 'on spawn', 'on spawn change',
        'on spread', 'on sprint toggle', 'on tame', 'on taming', 'on target', 'on teleport', 'on vehicle create',
        'on vehicle damage', 'on vehicle destroy', 'on vehicle enter', 'on vehicle exit', 'on weather change',
        'on world init', 'on world load', 'on world save', 'on world unload', 'on zombie break door'
    ];

    var skriptConditions = [
        'can', 'build', 'cannot', 'hold', 'space', 'chance', 'contains', 'contain',
        'have', 'damage', 'exists', 'has permission', 'has the permission',
        'has played', "hasn't played", 'is alive', 'is dead', 'banned', 'is blocking',
        'is burning', 'is empty', 'in enchanted', 'is not flying', 'is flying', 'is holding',
        "isn't holding", 'is the owner', 'is the member', 'online', 'offline',
        'poisoned', 'riding', 'script is loaded', 'sleeping', 'is sneaking', 'is not sprinting',
        'is sprinting', 'is wearing', "isn't wearing", 'is in', 'is of type', 'ground',
        'pvp is enabled', 'pvp is disabled', 'region', 'is thundering', 'is raining',
        'is sun'
    ];

    var openingKeywords = ['if', 'else', 'command', 'send', 'trigger', 'permission',
        'loop', 'quit', 'wait', 'to', 'delete', 'change', 'of', 'aliases', 'set', 'player',
        'function', 'or'
    ];

    var skEvents = wordRegexp(skriptEvents);
    var skConditions = wordRegexp(skriptConditions);
    var stringPrefixes = '"';

    var opening = wordRegexp(openingKeywords);
    var doubleClosing = wordRegexp(['end']);
    var doOpening = wordRegexp(['do']);

    var indentInfo = null;

    CodeMirror.registerHelper("hintWords", "vb", openingKeywords.concat(skriptEvents).concat(skriptConditions));

    function indent(_stream, state) {
      state.currentIndent++;
    }

    function dedent(_stream, state) {
      state.currentIndent--;
    }
    // tokenizers
    function tokenBase(stream, state) {
        if (stream.eatSpace()) {
            return null;
        }

        var ch = stream.peek();

        // Handle Comments
        if (ch === "#") {
            stream.skipToEnd();
            return 'comment';
        }


        // Handle Number Literals
        if (stream.match(/^((&H)|(&O))?[0-9\.a-f]/i, false)) {
            var floatLiteral = false;
            // Floats
            if (stream.match(/^\d*\.\d+F?/i)) { floatLiteral = true; }
            else if (stream.match(/^\d+\.\d*F?/)) { floatLiteral = true; }
            else if (stream.match(/^\.\d+F?/)) { floatLiteral = true; }

            if (floatLiteral) {
                // Float literals may be "imaginary"
                stream.eat(/J/i);
                return 'number';
            }
            // Integers
            var intLiteral = false;
            // Hex
            if (stream.match(/^&H[0-9a-f]+/i)) { intLiteral = true; }
            // Octal
            else if (stream.match(/^&O[0-7]+/i)) { intLiteral = true; }
            // Decimal
            else if (stream.match(/^[1-9]\d*F?/)) {
                // Decimal literals may be "imaginary"
                stream.eat(/J/i);
                // TODO - Can you have imaginary longs?
                intLiteral = true;
            }
            // Zero by itself with no other piece of number.
            else if (stream.match(/^0(?![\dx])/i)) { intLiteral = true; }
            if (intLiteral) {
                // Integer literals may be "long"
                stream.eat(/L/i);
                return 'number';
            }
        }

        // Handle Strings
        if (stream.match(stringPrefixes)) {
            state.tokenize = tokenStringFactory(stream.current());
            return state.tokenize(stream, state);
        }

        // Handle operators and Delimiters
        if (stream.match(tripleDelimiters) || stream.match(doubleDelimiters)) {
            return null;
        }
        if (stream.match(doubleOperators)
            || stream.match(singleOperators)) {
            return 'operator';
        }
        if (stream.match(singleDelimiters)) {
            return null;
        }
        
        // buradan başlıyor
        
        if (stream.match(doOpening)) {
            indent(stream,state);
            state.doInCurrentLine = true;
            return 'keyword';
        }
        if (stream.match(opening)) {
            if (! state.doInCurrentLine)
              indent(stream,state);
            else
              state.doInCurrentLine = false;
            return 'keyword';
        }

        if (stream.match(doubleClosing)) {
            dedent(stream,state);
            dedent(stream,state);
            return 'keyword';
        }

        if (stream.match(skEvents)) {
            return 'skEvents';
        }

        if (stream.match(skConditions)) {
            return 'skConditions'
        }

        if (stream.match(identifiers)) {
            return 'variable';
        }

        // Handle non-detected items
        stream.next();
        return ERRORCLASS;
    }

    function tokenStringFactory(delimiter) {
        var singleline = delimiter.length == 1;
        var OUTCLASS = 'string';

        return function(stream, state) {
            while (!stream.eol()) {
                stream.eatWhile(/[^'"]/);
                if (stream.match(delimiter)) {
                    state.tokenize = tokenBase;
                    return OUTCLASS;
                } else {
                    stream.eat(/['"]/);
                }
            }
            if (singleline) {
                if (parserConf.singleLineStringErrors) {
                    return ERRORCLASS;
                } else {
                    state.tokenize = tokenBase;
                }
            }
            return OUTCLASS;
        };
    }


    function tokenLexer(stream, state) {
        var style = state.tokenize(stream, state);
        var current = stream.current();

        // Handle '.' connected identifiers
        if (current === '.') {
            style = state.tokenize(stream, state);
            if (style === 'variable') {
                return 'variable';
            } else {
                return ERRORCLASS;
            }
        }


        var delimiter_index = '[({'.indexOf(current);
        if (delimiter_index !== delimiter_index) {
            indent(stream, state );
        }
        if (indentInfo === 'dedent') {
            if (dedent(stream, state)) {
                return ERRORCLASS;
            }
        }
        delimiter_index = '])}'.indexOf(current);
        if (delimiter_index !== -1) {
            if (dedent(stream, state)) {
                return ERRORCLASS;
            }
        }

        return style;
    }

    var external = {
        electricChars:"dDpPtTfFeE ",
        startState: function() {
            return {
              tokenize: tokenBase,
              lastToken: null,
              currentIndent: 0,
              nextLineIndent: 0,
              doInCurrentLine: false


          };
        },

        token: function(stream, state) {
            if (stream.sol()) {
              state.currentIndent += state.nextLineIndent;
              state.nextLineIndent = 0;
              state.doInCurrentLine = 0;
            }
            var style = tokenLexer(stream, state);

            state.lastToken = {style:style, content: stream.current()};



            return style;
        },

        indent: function(state, textAfter) {
            var trueText = textAfter.replace(/^\s+|\s+$/g, '') ;
            if(state.currentIndent < 0) return 0;
            return state.currentIndent * conf.indentUnit;
        },

        lineComment: "#"
    };
    return external;
});

CodeMirror.defineMIME("text/x-vb", "vb");

});
